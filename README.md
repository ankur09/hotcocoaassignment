# Assignment completed

## Getting Started

To create isolated python environment install virtualenv.It will create a folder which contain all the necessary executables to
use the packages that this project would need.

### Prerequisites

Activate your virtualenv to install all packages from requirement.txt


### Installing

```
pip install -r requirement.txt

```

## Built With

* [Django](https://docs.djangoproject.com/en/1.11/intro/tutorial01/) - The web framework used
* [MySQL](https://www.mysql.com/) - Database used



###  Configure your database


* Add your MySQL database configuration in .env file

```
DB_NAME=dbname
DB_USER=dbuser
DB_PASSWORD=dbpassword

```

### Running migrations

```
* python manage.py makemigrations
* python manage.py migrate

```

###  List Of API's

* User Registration

```
url = [http://127.0.0.1:8000/user/register]

method = POST

data = {
    username:username,
    email:email,
    password:password
    }

```
##### The activation link has been sent to your email address.

* User Login

```
url = [http://127.0.0.1:8000/user/login]

method = POST

data = {
    email:email,
    password:password
    }

```

##### Please save token for Authorization and that token should be pass in header

* Post your Text

```

    url = http://localhost:8000/user/post

    method = post

    Header = {
        Authorization : COCOA asdsffsfeffdfa-f0afdfafafdfassfadfasffdafadsdff
        }

    data = {
           text:your text
        }

```

* Post from all users searching,ordering,pagination (Implemented)

```
    Default:pageSize = 20


    * Example: search rahul posts

    Header = {
        Authorization : COCOA asdsffsfeffdfa-f0afdfafafdfassfadfasffdafadsdff
        }

    url = [http://localhost:8000/list?page=1&pageSize=10&search=rahul2&ordering=created_at]

```

 * Optional

 ```
    List of Post from that logged In User

    Header = {
        Authorization : COCOA asdsffsfeffdfa-f0afdfafafdfassfadfasffdafadsdff
        }
    url = [http://localhost:8000/user/post/list]

```






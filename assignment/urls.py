from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view
from accounts.views.userLoginLogoutViews import activate
from accounts.views.userTextPostViews import PostsByAllUsersAPIView
from rest_framework_jwt.views import obtain_jwt_token

schema_view = get_swagger_view(title='HotCocoa Software')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^swagger/$', schema_view),
    url(r'api-token-auth/$',obtain_jwt_token),
    url(r'^user/',include('accounts.urls',namespace='account')),
    url(r'^list$',PostsByAllUsersAPIView.as_view(),name='users-post'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),



]

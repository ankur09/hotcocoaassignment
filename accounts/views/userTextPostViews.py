from rest_framework import status
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.filters import (SearchFilter,OrderingFilter)

from accounts.models import simpleForm

from datetime import datetime

from accounts.pagination import PostPageNumberPagination
from accounts.serializers.userTextPostSerializers import UserTextPostSerializer, UserPostListSerializer, \
    PostsByAllUsersSerializers
from accounts.utils.constants import ResponseHandle

class TextPostAPIView(APIView):
    permission_classes = [IsAuthenticated]
    def post(self,request,format='json'):
        """
            Authenticated User can create a post
        """
        serializer = UserTextPostSerializer(data = request.data)
        if serializer.is_valid(raise_exception=True):
            simpleForm.objects.create(user = request.user,text = serializer.validated_data['text'],created_at = datetime.now(),modified_at=datetime.now())
            return Response(ResponseHandle.onSuccess('successfully created'),status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)




class UserPostListAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = UserPostListSerializer
    filter_backends = [SearchFilter,OrderingFilter]
    search_fields = ['text']
    def get_queryset(self,*args,**kwargs):
        queryset_list = simpleForm.objects.filter(user=self.request.user)
        if not queryset_list:
            return Response(ResponseHandle.onFailure('No Result Found'),status=status.HTTP_404_NOT_FOUND)
        q = self.request.GET.get('q')
        if q:
            queryset_list = queryset_list.filter(text__icontains=q)

        return queryset_list


class PostsByAllUsersAPIView(ListAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PostsByAllUsersSerializers
    filter_backends = [SearchFilter,OrderingFilter]
    search_fields = ['user__username']
    pagination_class = PostPageNumberPagination
    def get_queryset(self,*args,**kwargs):
        queryset_list = simpleForm.objects.all()
        if not queryset_list:
            return Response(ResponseHandle.onFailure('No Result Found'),status=status.HTTP_404_NOT_FOUND)
        q = self.request.GET.get('q')
        pageSize = self.request.GET.get('pageSize')
        try:
            if pageSize:
                self.pagination_class.page_size = pageSize
        except Exception as err:
            print (err)
        if q:
            queryset_list = queryset_list.filter(user__username__icontains=q)

        return queryset_list
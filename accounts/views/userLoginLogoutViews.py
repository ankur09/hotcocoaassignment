# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.http.response import HttpResponse
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

import json
from accounts.serializers.userLoginLogoutSerializer import UserLoginSerializer
from accounts.utils.token_manage import get_token_value
from django.contrib.sites.shortcuts import get_current_site
from accounts.utils.utility import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode


class LoginAPIView(APIView):
    permission_classes = [AllowAny]
    def post(self,request,format='json'):
        """
        Return a Valid token if username and password
        is valid for a given client
        """
        serializer = UserLoginSerializer(data = request.data)
        if serializer.is_valid(raise_exception=True):
            login(request,serializer.validated_data['user'])
            response = get_token_value(get_current_site(request).domain,{'username':serializer.validated_data['user'].username,'password': serializer.validated_data['password']})
            return Response(json.loads(response.text),status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)



def activate(request, uidb64, token):
    try:
        uid = int(force_text(urlsafe_base64_decode(uidb64)))
        user = User.objects.get(id=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user,token):
        user.is_active = True
        user.save()
        login(request, user)
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny



from accounts.serializers.userRegistrationSerializer import UserRegistrationSerializer



class UserCreateAPIView(CreateAPIView):
    """
    user registration api
        """
    permission_classes = [AllowAny]
    serializer_class = UserRegistrationSerializer
from django.contrib.auth.models import User
from django.http.response import HttpResponse
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.db import transaction
from django.contrib.sites.shortcuts import get_current_site

from accounts.utils.utility import send_email


class UserRegistrationSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(max_length=100,required=True)
    password = serializers.CharField(max_length=100,required=True)
    class Meta:
        model = User
        fields=['username','password','email']

    @transaction.atomic
    def create(self,data):
        user,_ = User.objects.get_or_create(username=data['username'],email=data['email'])
        if data.get('password'):
            user.set_password(data.get('password'))
            user.is_active = False
            user.save()
            send_email(get_current_site(self._context['request']).domain,user)
        return data
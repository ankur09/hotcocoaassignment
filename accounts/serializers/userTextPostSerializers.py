from rest_framework import serializers

from accounts.models import simpleForm



class UserTextPostSerializer(serializers.ModelSerializer):
    text = serializers.CharField(max_length=255,allow_blank=True,allow_null=True)
    class Meta:
        model = simpleForm
        fields = ['text']




class UserPostListSerializer(serializers.ModelSerializer):
    email = serializers.CharField(source='user.email')
    class Meta:
        model = simpleForm
        fields = ['text','email','created_at','modified_at']




class PostsByAllUsersSerializers(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username')
    email = serializers.EmailField(source='user.email')
    class Meta:
        model = simpleForm
        fields = ['username','email','text','created_at']
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import serializers

from accounts.models import simpleForm


class UserLoginSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=100,required=True)
    password = serializers.CharField(max_length=100,required=True)
    class Meta:
        model = User
        fields = ['email','password']

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')
        if email and password:
            try:
                user = User.objects.get(email=email)
            except:
                raise serializers.ValidationError("Oops email and password does not match")
            if not user.check_password(password):
                raise serializers.ValidationError("Oops email and password does not match")
            if user.is_active:
                data['user']= user
                return data
            else:
                raise serializers.ValidationError("account is inactive.Activate it by clicking on link that has been sent to your email address")
        else:
            raise serializers.ValidationError("email and password both required")
        return data





from django.core.mail import send_mail

from accounts.utils.constants import baseURL
from assignment import settings
from django.utils.crypto import get_random_string
import hashlib
from rest_framework.exceptions import APIException
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode





class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )
account_activation_token = TokenGenerator()



def send_email(domain,user):
    mail_subject = 'Activate your account'
    link= 'http://'+domain+'/activate/%s/%s/' %(urlsafe_base64_encode(force_bytes(user.id)),account_activation_token.make_token(user))
    from_email=settings.EMAIL_HOST_USER
    to_email=[from_email,user.email]
    try:
        return send_mail(mail_subject,link,from_email,to_email,fail_silently=False)
    except Exception as e:
        raise APIException(e)




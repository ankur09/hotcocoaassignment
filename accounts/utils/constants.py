

baseURL = 'http://localhost:8080/'

class ResponseHandle(object):
    @staticmethod
    def onSuccess(data):
        return {'status_code':'2XX','data':data}
    @staticmethod
    def onFailure(data):
        return {'status_code':'4XX','errorMessage':data}


import requests
import json

from assignment.settings import HEADERS


def get_token_value(domain,payload):
    url = 'http://'+domain + '/api-token-auth/'
    response = requests.post(url=url,data=json.dumps(payload),headers=HEADERS)
    return response

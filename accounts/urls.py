from django.conf.urls import url
from django.contrib import admin

from accounts.views.userLoginLogoutViews import LoginAPIView
from accounts.views.userRegistrationViews import UserCreateAPIView
from accounts.views.userTextPostViews import TextPostAPIView, UserPostListAPIView

urlpatterns = [

    url(r'^register$',UserCreateAPIView.as_view(),name='user-register'),
    url(r'^login$',LoginAPIView.as_view(),name='user-login'),
    url(r'^post$',TextPostAPIView.as_view(),name='user-post'),
    url(r'^post/list$',UserPostListAPIView.as_view(),name='user-post'),
]
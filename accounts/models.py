# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.


class simpleForm(models.Model):
    user = models.ForeignKey(User,related_name="users",help_text="")
    text = models.TextField(max_length=255,blank=True,null=True)
    created_at = models.DateTimeField(editable=False)
    modified_at = models.DateTimeField()


    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        return super(simpleForm, self).save(*args, **kwargs)


    def __unicode__(self):
        return unicode(self.text)
